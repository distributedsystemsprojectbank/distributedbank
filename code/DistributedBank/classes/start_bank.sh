#!/bin/bash
#rmiregistry &
javac -d . ../src/bank/Bank.java ../src/branches/BankBranch.java ../src/messages/Token.java ../src/messages/Transfer.java ../src/interfaces/BankInterface.java ../src/messages/Update.java ../src/snapshot/Snapshot.java
java -cp . -Djava.security.policy=server.policy bank.Bank $1 $2
