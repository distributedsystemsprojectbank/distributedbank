/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

import java.io.Serializable;


/**
 *
 * @author gimmi
 */
public class Transfer implements Serializable{
    public int money;
    public int seq_num;
    public int id;

    public Transfer(int money, int seq_num, int id) {
        this.money = money;
        this.seq_num = seq_num;
        this.id = id;
    }

}
