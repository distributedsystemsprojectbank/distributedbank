/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import branches.BankBranch;
import interfaces.BankInterface;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import static java.lang.Thread.sleep;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import messages.Token;

/**
 *
 * @author gimmi
 */
public class Bank {

    /**
     *
     */
    //main function of the project. it creates n servers, registers them on to the rmiregistry
    //and starts them on separate threads.
    //Finally it collects the results.
    public static Registry local_registry;
    public static Registry remote_registry;
    public static HashMap<Integer, BankBranch> bank_branches; //tracks the bank branches currently registered and running on this local system
    public static HashMap<Integer, BankBranch> all_branches; //tracks all the bank branches instantiated on this local system, also the ones removed during the execution
    public static HashMap<Integer, Thread> server_threads; //tracks the threads on which the bank branches are running
    public static HashMap<Integer, Integer> money_in_the_system;
    public static int current_money_in_the_system;
    public static HashMap<Integer, Integer> removed_banks; //tracks the bank branches removed during the program execution on this system
    public static int active_branches = 0;

    public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        System.setProperty("java.rmi.server.hostname", args[0]);
        try {
            //delete old log files
            File folder = new File("logs/");
            File[] listOfFiles = folder.listFiles();
            if (listOfFiles.length != 0) {
                for (File file : listOfFiles) {
                    if (file.isFile()) {
                        file.delete();
                    }
                }
            }
            //locate the local rmiregistry, where to register the branches instantiated on this system
            local_registry = LocateRegistry.createRegistry(1099);
            //initialize all the structures to keep track of the execution
            bank_branches = new HashMap<Integer, BankBranch>();
            all_branches = new HashMap<Integer, BankBranch>();
            money_in_the_system = new HashMap<Integer, Integer>();
            server_threads = new HashMap<Integer, Thread>();
            removed_banks = new HashMap<Integer, Integer>();
            current_money_in_the_system = 0;
            money_in_the_system.put(0, current_money_in_the_system);
            //int timeout = Integer.parseInt(args[2]); //specifies the timeout to wait before rentransmit a message

            //get the user input until 'stop' is typed
            Scanner scanner = new Scanner(System.in);
            System.out.println("Usage:\n"
                    + "Type 'start' to start the system and get the remote registry reference.");
            boolean running = true;
            int snapshots_launched = 0;
            while (true) {
                System.out.println("Type command:");
                String command = scanner.nextLine();
                if (command.equals("start")) {
                    //get the remote registry reference
                    remote_registry = LocateRegistry.getRegistry(args[1], 1099);
                    System.out.println("\nDistributed Bank is running...\n"
                            + "- Type 'add <branch_id>' to start one branch with given id.\n"
                            + "- Type 'add from <first_id> to <last_id>' to start n branches.\n"
                            + "- Type 'snap' to start a new snapshot.\n"
                            + "- Type 'list' to list the active branches. \n"
                            + "- Type 'stop' to stop the branches and the execution. \n");
                    //+ "- Type 'remove <branch_id>' to remove a branch with given id from the system.\n"
                } else if (command.equals("snap")) {
                    //repeat a snapshot for 'e' times
                    //for (int e = 0; e < 20; e++) {
                        snapshots_launched++;
                        //extract a random initiator
                        Random generator = new Random();
                        Object[] values = bank_branches.values().toArray();
                        BankBranch randomInitiator = (BankBranch) values[generator.nextInt(values.length)];
                        //create a new token with id = 0 and send it to the random initiator
                        Token token = new Token(0, snapshots_launched);
                        randomInitiator.receive_token(token);
                        System.out.println("Snapshot " + (snapshots_launched) + " started. The initiator is server " + randomInitiator.getID());
                        int total_active_branches = local_registry.list().length + remote_registry.list().length;
                        Thread.sleep(total_active_branches * 120);
//                    if(total_active_branches < 10) Thread.sleep(1000);
//                    else if(total_active_branches >= 20 && total_active_branches < 30) Thread.sleep(1500);
//                    else if(total_active_branches >=30 && total_active_branches < 50) Thread.sleep(2000);
//                    else Thread.sleep(total_active_branches * 150);
//                    }
                } else if (command.equals("stop")) {
                    ArrayList<Integer> b = new ArrayList<Integer>();
                    for (Map.Entry<Integer, BankBranch> entry : bank_branches.entrySet()) {
                        b.add(entry.getKey());
                    }
                    for (int g : b) {
                        remove_bank_to_stop_program(g);
                    }
                    stop_banks(server_threads, bank_branches);
                    running = false;
                    break;
                } else if (command.contains("add")) {
                    if (command.contains("from") && command.contains("to")) {
                        if (command.split(" ").length == 5) {
                            int first_id = Integer.parseInt(command.split(" ")[2]);
                            int last_id = Integer.parseInt(command.split(" ")[4]);
                            for (int i = first_id; i <= last_id; i++) {
                                add_bank_to_registry(i);
                            }
                        } else {
                            System.out.println("Unknown command. Type 'add from <first_id> to <last_id>'.");
                        }
                    } else {
                        if (command.split(" ").length == 2) {
                            int new_id = Integer.parseInt(command.split(" ")[1]);
                            add_bank_to_registry(new_id);
                        } else {
                            System.out.println("Unknown command. Type 'add bank_id'.");
                        }
                    }
//                } else if (command.contains("remove")) {
//                    if (command.split(" ").length < 1 || command.split(" ").length > 1) {
//                        int id = Integer.parseInt(command.split(" ")[1]);
//                        unbind_bank_from_registry(id);
//                    }
//                    else System.out.println("Unknown command. Type 'remove bank_id'.");
                } else if (command.equals("list")) {
                    System.out.println("Local registry: " + Arrays.toString(local_registry.list()));
                    System.out.println("Remote registry: " + Arrays.toString(remote_registry.list()));
                }
            }

            Thread.sleep(200);
            System.out.println("FINISHED");

            //retrieve the final amount of money in each bank to check if it is equal to the initial amount
            int tot = 0;
            for (Map.Entry<Integer, BankBranch> entry : bank_branches.entrySet()) {
                tot += entry.getValue().getBalance().get();
            }
//            int removed = 0;
//            int money_removed = 0;
//            if (removed_banks.size() > 0) {
//                for (Map.Entry<Integer, Integer> entry : removed_banks.entrySet()) {
//                    removed ++;
//                    money_removed += entry.getValue();
//                }
//                String q;
//                if(removed_banks.size() > 1) q = "bank";
//                else q = "banks";
//                
//                System.out.println("Final amount of money in this system: " + tot + "$. " + removed + " " +q + " left the system for a total amount of money of " + money_removed + "$.");
//            }
//            else 
            System.out.println("Final amount of money in this system: " + tot + "$");

            print_snapshot_results();
            System.exit(0);

        } catch (Exception e) {
            System.err.println("Main program exception:");
            e.printStackTrace();
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //register the servers on to the rmiregistry so to be reachable from remote
    public static void register_service(BankBranch branch, String branch_name) throws RemoteException, AlreadyBoundException {

        //create the stub
        BankInterface stub = (BankInterface) UnicastRemoteObject.exportObject(branch, 0);
        //bind the name of the bank with the remote reference of the object (the stub)
        local_registry.rebind(branch_name, stub);
        System.out.println("Branch " + branch_name + " registered.");

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void start_banks(HashMap<Integer, Thread> server_threads) {
        for (Map.Entry<Integer, Thread> entry : server_threads.entrySet()) {
            entry.getValue().start();
        }
    }

    public static void stop_banks(HashMap<Integer, Thread> server_threads, HashMap<Integer, BankBranch> branches) throws InterruptedException {
        for (Map.Entry<Integer, BankBranch> entry : all_branches.entrySet()) {
            entry.getValue().setSuspended(true);
            entry.getValue().setRunning(false);
        }
        for (Map.Entry<Integer, Thread> entry : server_threads.entrySet()) {
            entry.getValue().join();
            System.out.println(entry.getValue().getName() + " stopped.");
        }
    }

    public static void print_snapshot_results() throws FileNotFoundException, IOException {
        HashMap<Integer, Integer> results = new HashMap<Integer, Integer>();
        File folder = new File("logs/");
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                String file_name = file.getName();

                BufferedReader br = new BufferedReader(new FileReader("logs/" + file_name));
                String line = br.readLine();
                while (line != null) {
                    int snap_id = Integer.parseInt(line.split(" ")[0]);
                    int local_state = Integer.parseInt(line.split(" ")[1]);
                    int channels_state = Integer.parseInt(line.split(" ")[2]);
                    if (!results.containsKey(snap_id)) {
                        results.put(snap_id, local_state + channels_state);
                    } else {
                        int old_value = results.get(snap_id);
                        int new_value = old_value + local_state + channels_state;
                        results.put(snap_id, new_value);
                    }
                    line = br.readLine();
                }
                br.close();
            }
        }
        int snapshot_total = 0;
        for (Map.Entry<Integer, Integer> entry : results.entrySet()) {
            snapshot_total += entry.getValue();
            System.out.println("Snapshot " + entry.getKey() + ": " + entry.getValue() + "$.");
        }
        System.out.println("\nSnapshots total in this system: " + snapshot_total + "$");

    }

    public static boolean add_bank_to_registry(int id) throws FileNotFoundException, UnsupportedEncodingException, RemoteException, AlreadyBoundException, NotBoundException {
        if(id == 0){
            System.out.println("The id '0' is not allowed.");
            return false;
        }
        //check if the bank is already active. If so, return false.
        if (check_if_bank_is_already_active(id)) {
            System.out.println("The bank with id=" + id + " is already registered and it's currently active.");
            return false;
        } else {
            String branch_name = "" + id + "";
            BankBranch branch = new BankBranch(branch_name, id, 1000000, remote_registry, local_registry);
            register_service(branch, branch_name);
            Thread t = new Thread(branch);
            initialize_this_branch(branch);
            bank_branches.put(id, branch);
            server_threads.put(id, t);
            all_branches.put(id, branch);
            t.start();
            active_branches++;
            System.out.println("Branch " + branch_name + " started");
            return true;
        }
    }

    public static boolean unbind_bank_from_registry(int id) throws RemoteException, InterruptedException, NotBoundException {
        //if it exists a registered bank with given id, unbind it from the registry and signal it to the other banks 
        if (bank_branches.containsKey(id)) {
            BankBranch branch = bank_branches.get(id);
            branch.remove_other_banks_reference();
            sleep(500);
            branch.setRunning(false);
            bank_branches.remove(id);
            local_registry.unbind("" + id + "");
            UnicastRemoteObject.unexportObject(branch, true);
            removed_banks.put(id, branch.getBalance().get());
            System.out.println("Bank" + id + " removed from registry");
            active_branches--;
            return true;
        } else {
            System.out.println("Bank" + id + " is not registered.");
            return false;
        }
    }

    public static boolean remove_bank_to_stop_program(int id) throws RemoteException, InterruptedException, NotBoundException {
        //if exists a registered bank with given id, unbind it from the registry and signal it to the other banks 
        BankBranch branch = bank_branches.get(id);
        branch.remove_other_banks_reference();
        sleep(500);
        branch.setRunning(false);
        local_registry.unbind("" + id + "");
        UnicastRemoteObject.unexportObject(branch, true);
        System.out.println("Bank" + id + " removed from registry");
        return true;
    }

    public static void initialize_this_branch(BankBranch branch) throws RemoteException, NotBoundException {
        branch.initialize_other_banks_reference();
    }

    public static boolean check_if_bank_is_already_active(int id) throws RemoteException {
        for (Map.Entry<Integer, BankBranch> entry : bank_branches.entrySet()) {
            if (entry.getKey() == id) {
                return true;
            }
        }
        String[] remote_branches = remote_registry.list();
        for (String remote_branche : remote_branches) {
            if (remote_branche.equals(Integer.toString(id))) {
                return true;
            }
        }
        return false;
    }
}
