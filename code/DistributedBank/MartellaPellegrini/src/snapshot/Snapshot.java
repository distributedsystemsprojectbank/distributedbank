/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snapshot;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author gimmi
 */

//Collects the snapshot state of each bank branch
public class Snapshot {
    private int snapshot_id;
    private AtomicInteger state;
    private AtomicInteger channels_state;

    public Snapshot(AtomicInteger state, AtomicInteger channels_state, int id) {
        this.state = state;
        this.channels_state = channels_state;
        this.snapshot_id = id;
    }

    public AtomicInteger getChannels_state() {
        return channels_state;
    }

    public void setChannels_state(AtomicInteger channels_state) {
        this.channels_state = channels_state;
    }

    public AtomicInteger getState() {
        return state;
    }

    public void setState(AtomicInteger state) {
        this.state = state;
    }

    public int getSnapshot_id() {
        return snapshot_id;
    }

    public void setSnapshot_id(int snapshot_id) {
        this.snapshot_id = snapshot_id;
    }
    
}
