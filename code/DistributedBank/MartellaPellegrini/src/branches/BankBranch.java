/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package branches;

import interfaces.BankInterface;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import messages.Token;
import messages.Transfer;
import messages.Update;
import snapshot.Snapshot;

/**
 *
 * @author gimmi
 */
public class BankBranch implements BankInterface, Runnable {

    public boolean running = true;
    private boolean suspended = false;
    private int snap_id;
    private static int current_snap_initiator;
    private final AtomicBoolean first_token = new AtomicBoolean(true); //for each snapshot signals if it is the first token received for that snapshot
    private final AtomicInteger received_tokens = new AtomicInteger(0); //counts how many tokens I have received in the current snapshot

    private final int id;
    private volatile AtomicInteger balance; //atomic variable that represents the current balance of the bank branch. Must be updated atomically (CAS)
    private final ConcurrentHashMap<Integer, Integer> incoming_connections; //List of incoming connections (Server_id, last_sequence_number) for each of the other servers
    private final ConcurrentHashMap<Integer, Integer> outgoing_connections; //List of outgoing connections (Server_id, last_sequence_number) for each of the other servers
    private final ConcurrentHashMap<Integer, AtomicReference<String>> channels_states; //keep track of the channels state while the snapshot is taking place
    //private PrintWriter writer;
    public Registry remote_registry; //the remote rmi registry reference
    public Registry local_registry; //the local rmiregistry reference
    public ConcurrentHashMap<Integer, String> all_servers; //each bank branch keeps a list of all the others
    public ConcurrentHashMap<Integer, String> remote_servers; ////each bank branch keeps a list of the remote active branches
    public ConcurrentHashMap<Integer, String> local_servers; //each bank branch keeps a list of the active branches in the same system
    private Snapshot snapshot;
    private PrintWriter server_snapshot;
    private ExecutorService executor_token;
    private ExecutorService executor_money;

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public boolean isSuspended() {
        return this.suspended;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public Snapshot getSnapshot() {
        return snapshot;
    }

    public AtomicInteger getBalance() {
        return balance;
    }

    public BankBranch(String name, int id, int balance, Registry remote_registry, Registry local_registry) throws FileNotFoundException, UnsupportedEncodingException {
        this.id = id;
        this.balance = new AtomicInteger(balance);
        this.remote_registry = remote_registry;
        this.local_registry = local_registry;
        this.incoming_connections = new ConcurrentHashMap<Integer, Integer>();
        this.outgoing_connections = new ConcurrentHashMap<Integer, Integer>();
        this.channels_states = new ConcurrentHashMap<Integer, AtomicReference<String>>();
        this.all_servers = new ConcurrentHashMap<Integer, String>();
        this.remote_servers = new ConcurrentHashMap<Integer, String>();
        this.local_servers = new ConcurrentHashMap<Integer, String>();
        try {
            this.server_snapshot = new PrintWriter(new File("logs/Server" + this.id + "log.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //main function of each bank thread: each bank starts to send money to the others in a while loop.
    //when a snapshot is occurring, the bank stop sending money until it has sent all the tokens on all the otugoing channels
    public void start_transactions() throws InterruptedException {
        //check if I am the only one in the system and wait until at least one other bank joins the system
        while (this.remote_servers.isEmpty() && this.local_servers.isEmpty()) {
            Thread.sleep(500);
        }
        while (this.running) {
            if (this.suspended) { //I have received the first token of a snapshot, so stop sending money until I have sent all tokens
                Token token = new Token(this.id, this.snap_id); //create new token to send with my id and sequence number
                try {
                    process_first_token(this.id, token);
                } catch (RemoteException ex) {
                }
                this.setSuspended(false);
            }
            //generate a random amount of money to deliver and choose if send money to "local" or remote bank
            int q = 0;
            int money_to_transfer = 0;
            if(this.local_servers.size() > 0 && this.remote_servers.isEmpty()) q = 1;
            else if (this.remote_servers.size() > 0 && this.local_servers.isEmpty()) q = 2;
            else{
                Random rand = new Random();
                q = rand.nextInt(2) + 1;
                money_to_transfer = rand.nextInt(100) + 1;
            }
            BankInterface receiver;
            if (q == 1) { //send money to a bank in the same system
                Random generator = new Random();
                Object[] values = this.local_servers.values().toArray();
                //retrieve a random local bank by its name
                try {
                    String receiver_name = (String) values[generator.nextInt(values.length)];
                    receiver = (BankInterface) local_registry.lookup(receiver_name);
                    //if at this point I haven't received a token, continue and send the money to the chosen bank
                    if (!this.suspended) {
                        send_money(receiver, money_to_transfer);
                    }
                } catch (RemoteException ex) {
                } catch (NotBoundException ex) {
                } catch (IllegalArgumentException ex){
                }
            } else if (q == 2) { //send money to a remote bank
                Random generator = new Random();
                Object[] values = this.remote_servers.values().toArray();
                //retrieve a random remote bank by its name
                try {
                    String receiver_name = (String) values[generator.nextInt(values.length)];
                    receiver = (BankInterface) remote_registry.lookup(receiver_name);
                    //if at this point I haven't received a token, continue and send the money to the chosen bank
                    if (!this.suspended) {
                        send_money(receiver, money_to_transfer);
                    }
                } catch (RemoteException ex) {
                } catch (NotBoundException ex) {
                } catch (IllegalArgumentException ex){
                }
            } else {
                //do nothing. extracted local server but there are no local servers available or the same thing with remote servers. try next loop
            }
        }
        this.server_snapshot.close();
        Thread.sleep(4000); // wait for a while before exit...
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // "client side" request: here the server acts as a client to send 'money' to a remote server.
    // this function calls the remote function 'receive_money(Transfer)' defined in the servers' interface
    public void send_money(BankInterface receiver, int money) {
        int money_to_withdraw = 0;
        try {
            //get the receiver server ID
            int receiver_id = receiver.getID();
            //get the correct sequence number to include in the message from the list of the outgoing connections
            int seq_num = this.outgoing_connections.get(receiver_id);
            //create a new Transfer object with the money to send, the sequence number and the branch (this) id.
            Transfer transfer = new Transfer(money, seq_num, this.id);
            money_to_withdraw = money;
            if (!this.suspended) { //check if the bank is currently sending tokens
                //withdraw tot money from this account
                check_concurrent_updates(this.balance, money_to_withdraw, "w");
                //send money to the receiver
                int ack = 0;
                int attempts = 0;
                int timeout = (this.remote_registry.list().length + this.local_registry.list().length) * 120;
                while (ack != 1) {
                    if(attempts > 5) break;
                    executor_money = Executors.newSingleThreadExecutor();
                    SendMoney sendmoney = new SendMoney(receiver, transfer);
                    Future<Integer> future = executor_money.submit(sendmoney);
                    try {
                        ack = future.get(timeout, TimeUnit.MILLISECONDS);
                        executor_money.shutdown();
                    } catch (InterruptedException ex) {
                        executor_money.shutdownNow();
                    } catch (ExecutionException ex) {
                        executor_money.shutdownNow();
                    } catch (TimeoutException ex) {
                        System.err.println("timeout");
                        executor_money.shutdownNow();
                    }
                    if (ack == 1) { //transfer OK
                        this.outgoing_connections.put(receiver_id, seq_num + 1); //update the sequence number for the next transfer
                    }
                    attempts++;
                }
                if(attempts > 5){ //the chosen bank is unreachable,re-add the withdrawed money from my account
                    check_concurrent_updates(this.balance, money_to_withdraw, "a");
                }
            }
        } catch (RemoteException ex) {
            
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //the remote server interface implementation to RECEIVE moneys
    @Override
    public boolean receive_money(Transfer transfer) throws RemoteException {
        int sender_id = transfer.id;
        //check the seq. num for the at most once semantics
        int last_seq_num = this.incoming_connections.get(sender_id);
        int current_seq_num = transfer.seq_num;
        if (current_seq_num <= last_seq_num) { //same or older sequence number, the request is repeated; just send back true in case 
            return true;
        } else { //new sequence number, process as usual
            try {
                //first check if a snapshot is in place; if so, update the corresponding channel state
                if (this.snapshot != null && this.channels_states.get(sender_id).compareAndSet("UNMARKED", "UNMARKED")) {
                    //System.out.println(this.id + " received " + transfer.money + "$ from " + sender_id);
                    update_channel_state(transfer.money);
                }
                //in any case, add money to my account and return true
                check_concurrent_updates(this.balance, transfer.money, "a");
                this.incoming_connections.put(sender_id, current_seq_num);
                return true;
            } catch (Exception e) {
                return false;
            }
        }

    }

    //the remote server interface implementation to RECEIVE tokens
    @Override
    public boolean receive_token(Token token) throws RemoteException {
        if (this.first_token.compareAndSet(true, false)) { //if this is the first token I've received
            this.snap_id = token.seq_num;
            this.setSuspended(true); //signals to the main thread to stop sending money
            if (token.id != 0) { //if I'm not the initiator
                this.channels_states.get(token.id).compareAndSet("UNMARKED", "MARKED"); //set the channel from where the first token has arrived as MARKED (empty)
                this.snapshot = new Snapshot(new AtomicInteger(this.balance.get()), new AtomicInteger(0), token.seq_num); //save my state
                this.received_tokens.getAndIncrement(); //increment the number of tokens received
            } else {
                this.snapshot = new Snapshot(new AtomicInteger(this.balance.get()), new AtomicInteger(0), token.seq_num);
            }
        } else {
            //duplicate token has just arrived. Set the channel state as marked.
            this.channels_states.get(token.id).compareAndSet("UNMARKED", "MARKED");
            this.received_tokens.getAndIncrement(); //increment the number of tokens received
        }
        //if I have received all tokens, signal that the snapshot is finished ad write on the log file
        if (this.received_tokens.compareAndSet(this.all_servers.size(), 0)) {
            //System.out.println(this.name + " received all tokens.");
            this.server_snapshot.write("" + this.snapshot.getSnapshot_id() + " " + this.snapshot.getState().get() + " " + this.snapshot.getChannels_state().get() + "\n");
            this.snapshot = null;
            this.server_snapshot.flush();
            for (Map.Entry<Integer, String> entry : this.all_servers.entrySet()) {
                this.channels_states.put(entry.getKey(), new AtomicReference<String>("UNMARKED"));
            }
            this.first_token.compareAndSet(false, true);
        }
        return true;
    }

    @Override
    public int getID() throws RemoteException {
        return this.id;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public void process_first_token(int my_id, Token token) throws RemoteException, InterruptedException {
        Multicast_tokens m = new Multicast_tokens(this.all_servers, new Token(my_id, token.seq_num));
        executor_token = Executors.newSingleThreadExecutor();
        executor_token.submit(m);
        executor_token.shutdown();
        if (!executor_token.awaitTermination((this.all_servers.size()), TimeUnit.SECONDS)) {
            System.out.println("the snapshot failed or took too long to complete.");
            executor_token.shutdownNow();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void check_concurrent_updates(AtomicInteger balance, int money, String operation) {
        int current_balance = balance.get();
        if (operation.equals("w")) { //atomically withdraw money from my balance
            if (!balance.compareAndSet(current_balance, current_balance - money)) {
                //System.out.println("Withdrawal fails, try again");
                check_concurrent_updates(balance, money, operation);
            }
        } else { //atomically add money to my balance
            if (!balance.compareAndSet(current_balance, current_balance + money)) {
                //System.out.println("Add fails, try again");
                check_concurrent_updates(balance, money, operation);
            }
        }
    }

    public void update_channel_state(int money) {
        int snap_state = this.snapshot.getChannels_state().get();
        if (!this.snapshot.getChannels_state().compareAndSet(snap_state, snap_state + money)) {
            //System.out.println("cannot update channel state");
            update_channel_state(money);
        }
    }

    @Override
    public boolean update_bank_list(Update update) throws RemoteException {
        String operation = update.operation;
        int new_id = update.id;
        if (operation.equals("add")) {
            this.incoming_connections.put(new_id, 0);
            this.outgoing_connections.put(new_id, 1);
            this.channels_states.put(new_id, new AtomicReference<String>("UNMARKED"));
            if (update.remote_or_local.equals("local")) {
                this.local_servers.put(new_id, "" + new_id + "");
                this.all_servers.put(new_id, "local");
            } else {
                this.remote_servers.put(new_id, "" + new_id + "");
                this.all_servers.put(new_id, "remote");
            }
            return true;
        } else if (operation.equals("remove")) {
            if (update.remote_or_local.equals("local")) {
                this.local_servers.remove(new_id);
                this.all_servers.remove(new_id);
                this.channels_states.remove(new_id);
            } else {
                this.remote_servers.remove(new_id);
                this.all_servers.remove(new_id);
                this.channels_states.remove(new_id);
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean initialize_other_banks_reference() {
        String[] active_remote_branches = null;
        String[] active_local_branches = null;
        try {
            active_remote_branches = remote_registry.list();
            active_local_branches = local_registry.list();
        } catch (RemoteException ex) {
            System.err.println("Error listing");
            Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (active_remote_branches != null && active_remote_branches.length > 0) {
            for (String s : active_remote_branches) {
                int bank_id = Integer.parseInt(s);
                this.incoming_connections.put(bank_id, 0);
                this.outgoing_connections.put(bank_id, 1);
                this.channels_states.put(bank_id, new AtomicReference<String>("UNMARKED"));
                this.all_servers.put(bank_id, "remote");
                this.remote_servers.put(bank_id, s);
                BankInterface branch;
                try {
                    branch = (BankInterface) remote_registry.lookup(s);
                    branch.update_bank_list(new Update(this.id, "add", "remote"));
                } catch (RemoteException ex) {
                    System.err.println("remote exception");
                    Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NotBoundException ex) {
                    System.err.println("Not bound exception");
                    Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        for (String s : active_local_branches) {
            int bank_id = Integer.parseInt(s);
            if (this.id != bank_id) {
                this.incoming_connections.put(bank_id, 0);
                this.outgoing_connections.put(bank_id, 1);
                this.channels_states.put(bank_id, new AtomicReference<String>("UNMARKED"));
                this.all_servers.put(bank_id, "local");
                this.local_servers.put(bank_id, s);
                BankInterface branch;
                try {
                    branch = (BankInterface) local_registry.lookup(s);
                    branch.update_bank_list(new Update(this.id, "add", "local"));
                } catch (RemoteException ex) {
                    System.err.println("remote exception");
                    Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NotBoundException ex) {
                    System.err.println("Not bound exception");
                    Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return true;
    }

    public boolean remove_other_banks_reference() {
        String[] active_remote_branches = null;
        String[] active_local_branches = null;
        try {
            active_local_branches = local_registry.list();
        } catch (RemoteException ex) {
            System.err.println("Error listing");
            Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (active_remote_branches != null && active_remote_branches.length > 0) {
            for (String s : active_remote_branches) {
                int bank_id = Integer.parseInt(s);
                BankInterface branch;
                try {
                    branch = (BankInterface) remote_registry.lookup(s);
                    branch.update_bank_list(new Update(this.id, "remove", "remote"));
                } catch (RemoteException ex) {
                    System.err.println("remote exception");
                    Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NotBoundException ex) {
                    System.err.println("Not bound exception");
                    Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        for (String s : active_local_branches) {
            int bank_id = Integer.parseInt(s);
            if (this.id != bank_id) {
                BankInterface branch;
                try {
                    branch = (BankInterface) local_registry.lookup(s);
                    branch.update_bank_list(new Update(this.id, "remove", "local"));
                } catch (RemoteException ex) {
                    System.err.println("remote exception");
                    Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NotBoundException ex) {
                    System.err.println("Not bound exception");
                    Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return true;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public class Multicast_tokens implements Runnable {

        private final Token tok;
        private final ConcurrentHashMap<Integer, String> branches_to_send_token;

        public Multicast_tokens(ConcurrentHashMap<Integer, String> receivers, Token token) {
            this.tok = token;
            this.branches_to_send_token = receivers;
        }

        @Override
        public void run() {
            try {
                for (Map.Entry<Integer, String> entry : branches_to_send_token.entrySet()) {
                    String receiverName = entry.getKey().toString();
                    //System.out.println(my_name + " send token to: " + receiverName);
                    String local_or_remote = entry.getValue();
                    if (local_or_remote.equals("local")) {
                        BankInterface receiver = (BankInterface) local_registry.lookup(receiverName);
                        receiver.receive_token(tok);
                    } else {
                        BankInterface receiver = (BankInterface) remote_registry.lookup(receiverName);
                        receiver.receive_token(tok);
                    }
                }
            } catch (RemoteException ex) {
                Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotBoundException ex) {
                Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public class SendMoney implements Callable {

        private final Transfer transfer;
        private final BankInterface receiver;

        public SendMoney(BankInterface receiver, Transfer transfer) {
            this.transfer = transfer;
            this.receiver = receiver;
        }

        @Override
        public Object call() throws Exception {
            if (receiver.receive_money(transfer)) {
                return 1;
            } else {
                return 0;
            }
        }
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void run() {
        try {
            start_transactions();
        } catch (InterruptedException ex) {
            Logger.getLogger(BankBranch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
