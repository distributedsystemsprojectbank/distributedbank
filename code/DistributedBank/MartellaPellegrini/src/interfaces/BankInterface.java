/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import messages.Token;
import messages.Transfer;
import messages.Update;

/**
 *
 * @author gimmi
 */
public interface BankInterface extends Remote {
    public boolean receive_money(Transfer transfer) throws RemoteException;
    public boolean receive_token(Token token) throws RemoteException;
    public int getID() throws RemoteException;
    public boolean update_bank_list(Update update) throws RemoteException;
}
