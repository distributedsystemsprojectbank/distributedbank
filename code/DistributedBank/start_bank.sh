#!/bin/bash

[ ! -d classes/  ] && mkdir classes

javac -d classes/ src/bank/Bank.java src/branches/BankBranch.java src/messages/Token.java src/messages/Transfer.java src/interfaces/BankInterface.java src/messages/Update.java src/snapshot/Snapshot.java
java -cp classes/ -Djava.security.policy=server.policy bank.Bank $1 $2
