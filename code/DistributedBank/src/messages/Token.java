/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

import java.io.Serializable;

/**
 *
 * @author gimmi
 */
public class Token implements Serializable{
    public int id;
    public int seq_num;
    
    public Token(int id, int seq_num){
        this.id = id;
        this.seq_num = seq_num;
    }
}
