/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

import java.io.Serializable;

/**
 *
 * @author gimmi
 */
public class Update implements Serializable{
    public int id;
    public String operation;
    public String remote_or_local;

    public Update(int id, String operation, String remote_or_local) {
        this.id = id;
        this.operation = operation;
        this.remote_or_local = remote_or_local;
    }
    
}
